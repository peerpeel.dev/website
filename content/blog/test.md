---
date: 2018-09-23T23:43:41-05:00
draft: true
title: "Implementación Real Time"
summary: "Te explicamos cómo integramos funcionalidad en real time, utilizando Elixir y Phoenix."
tags: ["test"]
categories: ["test"]
author: "Juan Pablo Yamamoto"
image: "/blog_img/1/phoenix.png"
---

En PeerPeel es de mayor prioridad para nosotros que el servicio funcione de manera óptima, sin latencia al momento de notificar a los usuarios de aquello que es importante. Por eso mismo, decidimos implementar el protocolo WebSocket que hoy en día tiene buen soporte en una amplia cantidad de navegadores.

## Backend
Tras nuestra experiencia, hemos concluido que Phoenix es la solución adecuada para lo que podemos hacer. Este es un framework para la web, creado en Elixir: un lenguaje con capacidades asíncronas, diseñado para funcionar en servicios con demasiado tráfico, de manera muy robusta y confiable. Phoenix nos proveé de los llamados Channels, que nos servirán para preparar los web sockets en el backend, algo que en muchos otros frameworks requeriría de mucha complejidad.

## Frontend
Debido a la integración de la API para websockets en los navegadores modernos, es realmente sencillo sacar provecho de ellos. En nuestro caso, esto es aún más simple, gracias a que utilizamos el ya mencionado Phoenix. Este framework nos proveé de una librería JS para utilizar en el frontend, y poder integrar de forma casi natural esta tecnología en cualquier proyecto. Lo único que necesitamos es un transpilador, como Babel, que nos permita hacer uso de la sintáxis EcmaScript 6. Esto significa que podemos añadirlo sin problemas a las dependencias de proyectos como Angular, React, o cualquier otro framework que tenga un bundler como Webpack, Brunch, Gulp, entre otros.

## Web Sockets
Si no te es familiar el término de los web sockets, podemos definirlo como un protocolo (justo como HTTP, FTP, etc.) que nos brinda un medio de comunicación entre dos dispositivos, que se puede mantener activo sin la necesidad de realizar consultas constántemente (como sería el caso de AJAX con HTTP). En nuestro caso, utilizamos los web sockets para avisar a los clientes de distintos eventos que deben ser notificados. Phoenix nos permite lograr lo anterior, gracias a la integración intrínseca de los endpoints REST con los endpoints asíncronos de los Web Sockets. Al recibir un evento a través de un endpoint HTTP que queremos comunicar a los web sockets, podemos simplemente dirigir un mensaje a un canal que hemos establecido, y consecuentemente el mensaje se transmite a todas las conexiones que existan en los web sockets.

Todo esto puede ser algo complejo de entender si uno no está familiarizado con los varios conceptos, sin embargo, te podemos asegurar que nuestro sistema está basado en las tecnologías más asertadas para las distintas tareas a realizar.

